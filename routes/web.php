<?php

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::get('/', 'UrlController@create')->name('url.create');
Route::get('/{url}', 'UrlController@redirect')->name('url.redirect');

Route::post('/store', 'UrlController@store')->name('url.store')->middleware(\App\Http\Middleware\LimitGuestAccess::class);

Route::get('/ip/{ip}', 'IpController@show')->name('ip.show')->middleware(['auth', 'can:manage-ip,ip']);
