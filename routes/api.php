<?php

use Illuminate\Http\Request;



Route::middleware('auth:Api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('store/{user}', 'Api\UrlController@store')->name('api.store');
