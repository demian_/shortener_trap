<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class LimitGuestAccess
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guest()) {
            $count = $request->session()->get('visit_count', 0);

            if ($count > 4) {
                return redirect()->route('url.create')->with('danger', 'You have a limit today');
            }

            $request->session()->put('visit_count', ++$count);
        }

        return $next($request);
    }
}
