<?php

namespace App\Http\Controllers;

use App\Models\Ip;
use App\Services\IpClient;

class IpController extends Controller
{
    private $ipClient;

    public function __construct(IpClient $ipClient)
    {
        $this->ipClient = $ipClient;
    }


    public function show(Ip $ip)
    {
        $response = $this->ipClient->getIpInfo($ip);

        return view('ip.show', compact('response'));
    }
}
