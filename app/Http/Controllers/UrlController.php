<?php

namespace App\Http\Controllers;

use App\Http\Requests\UrlRequest;
use App\Models\Url;
use App\Models\User;
use App\Services\UrlService;
use Illuminate\Http\Request;

class UrlController extends Controller
{
    private $urlService;

    public function __construct(UrlService $urlService)
    {
        $this->urlService = $urlService;
    }


    public function create()
    {
        return view('create');
    }

    public function store(UrlRequest $request)
    {
        $generatedUrl = $this->urlService->create($request, \Auth::user());

        return redirect()->route('url.create')
            ->with('success', 'Link has been generated: ' . config('app.url') . '/' . $generatedUrl->hash);
    }

    public function redirect(Url $url, Request $request)
    {
        if ($url->isExpired()) {
            abort(404);
        }
        $this->urlService->setVisitedIp($url, $request->ip());

        return view('url.redirect_page', compact("url"));
    }
}
