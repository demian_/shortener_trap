<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UrlRequest;
use App\Models\User;
use App\Services\UrlService;
use Illuminate\Http\Request;

class UrlController extends Controller
{
    private $urlService;

    public function __construct(UrlService $urlService)
    {
        $this->urlService = $urlService;
    }

    public function store(UrlRequest $request, User $user)
    {
        $generatedUrl = $this->urlService->create($request, $user);


        return response()->json([
            'url' => $request->input('url'),
            'path' => config('app.url') . '/' . $generatedUrl->hash
        ]);
    }
}
