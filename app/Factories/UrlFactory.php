<?php


namespace App\Factories;


use App\Models\Url;
use Carbon\Carbon;
use \Symfony\Component\HttpFoundation\Request;
use App\Models\User;
use Illuminate\Support\Str;

class UrlFactory
{
    public function create(Request $request, ?User $user = null)
    {
        $url = Url::make([
            'path' => $request['url'],
            'hash' => Str::random(10),
            'expires_at' => Carbon::now()->addHours(5),
        ]);
        if ($user) {
            $url->user()->associate($user);
        }
        $url->save();

        return $url;
    }
}
