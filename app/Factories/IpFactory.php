<?php


namespace App\Factories;


use App\Models\Ip;
use App\Models\Url;
use App\Models\User;

class IpFactory
{
    public function create(Url $url, $visitedIp)
    {
        /** @var Ip $ip */
        $ip = Ip::make([
            'adress' => $visitedIp,
        ]);
        $ip->url()->associate($url);
        $ip->save();

        return $url;
    }
}
