<?php


namespace App\Services;

use App\Models\Ip;
use GuzzleHttp\Client;


class IpClient
{
    private $httpClient;

    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getIpInfo(Ip $ip)
    {
        $url = config('external_api.ip_location') . $ip->adress;
        $response = $this->httpClient->request('GET', $url);

        return $this->formatResponse($response->getBody());
    }

    private function formatResponse(string $response)
    {
        $arrayResponse = json_decode($response, true);
        return $arrayResponse['data']['geo'];
    }
}
