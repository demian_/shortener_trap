<?php


namespace App\Services;


use App\Factories\IpFactory;
use App\Factories\UrlFactory;
use App\Models\User;
use \Symfony\Component\HttpFoundation\Request;
use App\Models\Url;
use DB;

class UrlService
{
    private $ipFactory;
    private $urlFactory;

    public function __construct(IpFactory $ipFactory, UrlFactory $urlFactory)
    {
        $this->ipFactory = $ipFactory;
        $this->urlFactory = $urlFactory;
    }


    public function create(Request $request, ?User $user)
    {
        $url = DB::transaction(function () use ($request, $user) {
            return $this->urlFactory->create($request, $user);
        });

        return $url;
    }

    public function setVisitedIp(Url $url, $ip)
    {
        if ($url->hasUser()) {
            $this->ipFactory->create($url, $ip);
        }
    }
}
