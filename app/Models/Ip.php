<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
/**
 * @property int $id
 * @property string $adress
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Url $url
 */
class Ip extends Model
{
    protected $guarded = [];

    public function url()
    {
        return $this->belongsTo(Url::class);
    }
}
