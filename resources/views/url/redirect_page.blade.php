@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="d-flex justify-content-center">
                Your ad could be here.
            </div>
            <div class="col">
                <div class="row d-flex justify-content-center">
                    <div id="spinner" class="spinner-grow text-info" role="status">
                    </div>
                    <h3><span id="time">00:10</span></h3>
                </div>
                <div class="row d-flex justify-content-center p-2">
                    <img src="https://re-media.plektan.com/img/prop/40/7f/407f503405e4ad187970a5873d2756a0.jpg"
                         width="500" alt="">
                </div>
            </div>
        </div>
    </div>
    <script>
        var path = "{{ $url->path }}";
    </script>
    <script src={{ asset('js/redirect.js') }}?v={{ md5_file(public_path('js/redirect.js')) }}>
    </script>
@endsection
