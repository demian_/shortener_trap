@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Home</div>
            <div class="card-body">
                <div id="accordion">
                    @forelse($urls as $url)
                        <div class="card">
                            <div class="card-header" id="heading_{{ $url->id }}">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse"
                                            data-target="#collapse_{{ $url->id }}"
                                            aria-expanded="true" aria-controls="collapse_{{ $url->id }}">
                                        {{ strlen($url->path) > 50 ? substr($url->path,0,50)."..." : $url->path }}
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse_{{ $url->id }}" class="collapse" aria-labelledby="heading_{{ $url->id }}"
                                 data-parent="#accordion">
                                <div class="card-body">
                                    <ul class="list-group">
                                        <li class="list-group-item">URL: {{ $url->path }}</li>
                                        <li class="list-group-item">Short
                                            link: {{ config('app.url') . '/' . $url->hash }}</li>
                                        <li class="list-group-item">IPs:
                                            @forelse($url->ips as $ip)
                                                <a href="{{ route('ip.show', $ip) }}">{{ $ip->adress }}</a>
                                                @if(!$loop->last), @endif
                                            @empty
                                                <p>No IPs yet.</p>
                                            @endforelse
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p>No links yet.</p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
@endsection
