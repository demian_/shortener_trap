@extends('layouts.app')

@section('content')
    <div class="container h-100">
        <div class="row align-items-center h-100">
            <div class="col-6 mx-auto">
                @include('layouts._flashes')
                <div class="jumbotron">
                    <form action="{{ route('url.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="url">URL</label>
                            <input type="text" class="form-control" id="url" autocomplete="off" name="url"
                                   value="{{ old('url') }}" required>
                            @error('url')
                            <p class="text-danger"><small>{{ $message }}</small></p>
                            @enderror
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-outline-primary">
                                Submit
                                {{--<i class="fa fa-spinner fa-spin"></i> Loading--}}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
