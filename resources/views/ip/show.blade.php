@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">{{ $response['ip'] }}</div>
            <div class="card-body">
                <p>Country: {{ $response['country_name'] }}</p>
                <p>Region: {{ $response['region_name'] }}</p>
                <p>City: {{ $response['city'] }}</p>
            </div>
        </div>
    </div>
@endsection
