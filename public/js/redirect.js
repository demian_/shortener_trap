function startTimer(duration, display) {
    var timer = duration, seconds;

    setInterval(function () {
        if (timer >= 0) {
            seconds = parseInt(timer % 60, 10);

            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.textContent = "00:" + seconds;

            if (timer === 0) {
                window.location.href = path;
            }
            --timer;
        }
    }, 1000);
}

window.onload = function () {
    var time = 10,
        display = document.querySelector('#time');
    startTimer(time, display);
};
